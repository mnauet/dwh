Please solve these questions on DEPT and EMPLOYEE data set available in Dice DB

Question#1: The employees form up a line grouped by their position. Inside each position group the employees order themselves by name. Every employee must have a unique number for its place in the line.

SELECT	EMPLOYEE_ID, EMPLOYEE_NAME, DEPT_ID, POSITION_, SALARY,
row_number() over (order by position_, employee_name asc) as sequence_no
from Dice.EMPLOYEE

Question#2: We would like to find the highest paid employees. Order all the employees by their salary. The two highest paid employees should both be tagged the 1st and the next highly paid to be as 3rd & so on.

SELECT	EMPLOYEE_ID, EMPLOYEE_NAME, DEPT_ID, POSITION_, SALARY,
rank() over (order by salary desc) as ranking
from Dice.EMPLOYEE

Question#3: Who is my 3rd highest paid employee?

SELECT	EMPLOYEE_ID, EMPLOYEE_NAME, DEPT_ID, POSITION_, SALARY,
rank() over (order by salary desc) as ranking
from Dice.EMPLOYEE
Qualify ranking =3

Question#4: Who are my top 2 highly paid employees for both AM & Manager positions?

SELECT	EMPLOYEE_ID, EMPLOYEE_NAME, DEPT_ID, POSITION_, SALARY,
rank() over (partition by position_ order by salary desc) as ranking
from Dice.EMPLOYEE
Qualify ranking <3

Question#5: Find the total salary of employees ordered by position & department. But only return those groups with a total salary larger than 25000, the result set to be sorted by lowest salary at the top.

SELECT	DEPT_ID, POSITION_, sum(SALARY) as total_sal
from Dice.EMPLOYEE
group by Dept_id, position_
having total_sal > 25000
order by total_sal asc;

Question#6: We need to find out the total compensated average salary within in each position w.r.t its department IDs. Show employee names, there employee IDs, salary & average salaries.

SELECT	EMPLOYEE_ID, EMPLOYEE_NAME, DEPT_ID, POSITION_, SALARY,
avg(salary) over (partition by dept_id,position_)
FROM Dice.EMPLOYEE