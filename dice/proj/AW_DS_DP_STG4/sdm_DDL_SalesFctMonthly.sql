CREATE TABLE Dp_SDM.SalesFctMonthly
(
		BusinessEntityID	INTEGER,
		OrderDate	DATE  FORMAT 'MM/DD/YYYY',
		DueDate	DATE FORMAT 'MM/DD/YYYY',
		ShipDate	DATE  FORMAT 'MM/DD/YYYY',
		AccountNumber	VARCHAR(16),
		TerritoryID	INTEGER,
		SubTotal	DECIMAL (10,2),
		TaxAmt	DECIMAL (10,2),
		Freight	DECIMAL (10,2),
		TotalDue	DECIMAL (10,2)
	)
	PRIMARY INDEX (BusinessEntityID);
/*
	
CREATE TABLE dp_stg2.SalesOrderDetail
	(
		BusinessEntityID INTEGER,
		SalesOrderID INTEGER,
		SalesOrderDetailID INTEGER,
		CarrierTrackingNumber VARCHAR(16),
		 OrderQty INTEGER,
		 ProductID INTEGER,
		 SpecialOfferID INTEGER,
		 UnitPrice DECIMAL(10,2),
		 UnitPriceDiscount  DECIMAL(6,2),
		 LineTotal DECIMAL(10,2)
	 )
	PRIMARY INDEX (SalesOrderDetailID,SalesOrderID,BusinessEntityID);

CREATE TABLE dp_stg2.SalesOrderHeader
	(
		BusinessEntityID	INTEGER,
		SalesOrderID	INTEGER,
		ShipMethodID	INTEGER,
		RevisionNumber	INTEGER,
		OrderDate	DATE  FORMAT 'MM/DD/YYYY',
		DueDate	DATE FORMAT 'MM/DD/YYYY',
		ShipDate	DATE  FORMAT 'MM/DD/YYYY',
		Status	BYTEINT,
		OnlineOrderFlag	BYTEINT,
		SalesOrderNumber	VARCHAR(16),
		PurchaseOrderNumber	VARCHAR(16),
		AccountNumber	VARCHAR(16),
		TerritoryID	INTEGER,
		BillToAddressID	INTEGER,
		ShipToAddressID	INTEGER,
		CreditCardID INTEGER,
		CreditCardApprovalCode VARCHAR(32),
		CurrencyRateID	INTEGER,
		SubTotal	DECIMAL (10,2),
		TaxAmt	DECIMAL (10,2),
		Freight	DECIMAL (10,2),
		TotalDue	DECIMAL (10,2)
	)
	PRIMARY INDEX (SalesOrderID,BusinessEntityID);
*/