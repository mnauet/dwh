-

--Sales

INSERT INTO DP_STG2.CreditCard	
SELECT DISTINCT 
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID,
	CASE WHEN CardType='NULL' THEN 'N/A' ELSE CardType END  AS CardType,
	CASE WHEN CardNumber = 'NULL' THEN 'NA' ELSE CardNumber END AS CardNumber,
	CASE WHEN ExpMonth='NULL' THEN -1 ELSE CAST(ExpMonth AS SMALLINT) END AS  ExpMonth,
	CASE WHEN ExpYear='NULL' THEN -1 ELSE CAST(ExpYear AS SMALLINT) END AS  ExpYear
FROM Dp_Stg1.Sales
UNION
SELECT DISTINCT 
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID,
	CASE WHEN CardType='NULL' THEN 'N/A' ELSE CardType END  AS CardType,
	CASE WHEN CardNumber = 'NULL' THEN 'NA' ELSE CardNumber END AS CardNumber,
	CASE WHEN ExpMonth='NULL' THEN -1 ELSE CAST(ExpMonth AS SMALLINT) END AS  ExpMonth,
	CASE WHEN ExpYear='NULL' THEN -1 ELSE CAST(ExpYear AS SMALLINT) END AS  ExpYear
FROM Dp_Stg1.Sales_April_1_10
UNION
SELECT DISTINCT 
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID,
	CASE WHEN CardType='NULL' THEN 'N/A' ELSE CardType END  AS CardType,
	CASE WHEN CardNumber = 'NULL' THEN 'NA' ELSE CardNumber END AS CardNumber,
	CASE WHEN ExpMonth='NULL' THEN -1 ELSE CAST(ExpMonth AS SMALLINT) END AS  ExpMonth,
	CASE WHEN ExpYear='NULL' THEN -1 ELSE CAST(ExpYear AS SMALLINT) END AS  ExpYear
FROM Dp_Stg1.Sales_April_11_20
UNION
SELECT DISTINCT 
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID,
	CASE WHEN CardType='NULL' THEN 'N/A' ELSE CardType END  AS CardType,
	CASE WHEN CardNumber = 'NULL' THEN 'NA' ELSE CardNumber END AS CardNumber,
	CASE WHEN ExpMonth='NULL' THEN -1 ELSE CAST(ExpMonth AS SMALLINT) END AS  ExpMonth,
	CASE WHEN ExpYear='NULL' THEN -1 ELSE CAST(ExpYear AS SMALLINT) END AS  ExpYear
FROM Dp_Stg1.Sales_April_21_30
UNION
SELECT DISTINCT 
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID,
	CASE WHEN CardType='NULL' THEN 'N/A' ELSE CardType END  AS CardType,
	CASE WHEN CardNumber = 'NULL' THEN 'NA' ELSE CardNumber END AS CardNumber,
	CASE WHEN ExpMonth='NULL' THEN -1 ELSE CAST(ExpMonth AS SMALLINT) END AS  ExpMonth,
	CASE WHEN ExpYear='NULL' THEN -1 ELSE CAST(ExpYear AS SMALLINT) END AS  ExpYear
FROM Dp_Stg1.Sales_May_06_10
UNION
SELECT DISTINCT 
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID,
	CASE WHEN CardType='NULL' THEN 'N/A' ELSE CardType END  AS CardType,
	CASE WHEN CardNumber = 'NULL' THEN 'NA' ELSE CardNumber END AS CardNumber,
	CASE WHEN ExpMonth='NULL' THEN -1 ELSE CAST(ExpMonth AS SMALLINT) END AS  ExpMonth,
	CASE WHEN ExpYear='NULL' THEN -1 ELSE CAST(ExpYear AS SMALLINT) END AS  ExpYear
FROM Dp_Stg1.Sales_May_11_20
UNION
SELECT DISTINCT 
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID,
	CASE WHEN CardType='NULL' THEN 'N/A' ELSE CardType END  AS CardType,
	CASE WHEN CardNumber = 'NULL' THEN 'NA' ELSE CardNumber END AS CardNumber,
	CASE WHEN ExpMonth='NULL' THEN -1 ELSE CAST(ExpMonth AS SMALLINT) END AS  ExpMonth,
	CASE WHEN ExpYear='NULL' THEN -1 ELSE CAST(ExpYear AS SMALLINT) END AS  ExpYear
FROM Dp_Stg1.Sales_May_21_31;

INSERT INTO DP_STG2.Currency
SELECT DISTINCT 
	CASE WHEN CurrencyCode = 'NULL' THEN -1 ELSE CAST(CurrencyCode AS CHAR(6)) END AS  CurrencyCode,
	CASE WHEN Name2 = 'NULL' THEN 'N/A' ELSE Name2 END AS Name1
FROM Dp_Stg1.Sales_May_06_10;

INSERT INTO DP_STG2.CurrencyRate
SELECT DISTINCT 
	CASE WHEN CurrencyRateID1='NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID1,
	CASE WHEN CurrencyRateDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(CurrencyRateDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS CurrencyRateDate,
	CASE WHEN FromCurrencyCode = 'NULL' THEN '-1' ELSE FromCurrencyCode END AS FromCurrencyCode,
	CASE WHEN ToCurrencyCode = 'NULL' THEN '-1' ELSE ToCurrencyCode END AS ToCurrencyCode,
	CASE WHEN AverageRate='NULL' THEN -1 ELSE CAST(AverageRate AS DECIMAL(8,2)) END AS AverageRate,
	CASE WHEN EndOfDayRate='NULL' THEN -1 ELSE CAST(EndOfDayRate AS DECIMAL(8,2)) END AS EndOfDayRate
FROM Dp_Stg1.Sales_April_1_10
UNION
SELECT DISTINCT 
	CASE WHEN CurrencyRateID1='NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID1,
	CASE WHEN CurrencyRateDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(CurrencyRateDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS CurrencyRateDate,
	CASE WHEN FromCurrencyCode = 'NULL' THEN '-1' ELSE FromCurrencyCode END AS FromCurrencyCode,
	CASE WHEN ToCurrencyCode = 'NULL' THEN '-1' ELSE ToCurrencyCode END AS ToCurrencyCode,
	CASE WHEN AverageRate='NULL' THEN -1 ELSE CAST(AverageRate AS DECIMAL(8,2)) END AS AverageRate,
	CASE WHEN EndOfDayRate='NULL' THEN -1 ELSE CAST(EndOfDayRate AS DECIMAL(8,2)) END AS EndOfDayRate
FROM Dp_Stg1.Sales_April_11_20
UNION
SELECT DISTINCT 
	CASE WHEN CurrencyRateID1='NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID1,
	CASE WHEN CurrencyRateDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(CurrencyRateDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS CurrencyRateDate,
	CASE WHEN FromCurrencyCode = 'NULL' THEN '-1' ELSE FromCurrencyCode END AS FromCurrencyCode,
	CASE WHEN ToCurrencyCode = 'NULL' THEN '-1' ELSE ToCurrencyCode END AS ToCurrencyCode,
	CASE WHEN AverageRate='NULL' THEN -1 ELSE CAST(AverageRate AS DECIMAL(8,2)) END AS AverageRate,
	CASE WHEN EndOfDayRate='NULL' THEN -1 ELSE CAST(EndOfDayRate AS DECIMAL(8,2)) END AS EndOfDayRate
FROM Dp_Stg1.Sales_April_21_30
UNION
SELECT DISTINCT 
	CASE WHEN CurrencyRateID1='NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID1,
	CASE WHEN CurrencyRateDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(CurrencyRateDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS CurrencyRateDate,
	CASE WHEN FromCurrencyCode = 'NULL' THEN '-1' ELSE FromCurrencyCode END AS FromCurrencyCode,
	CASE WHEN ToCurrencyCode = 'NULL' THEN '-1' ELSE ToCurrencyCode END AS ToCurrencyCode,
	CASE WHEN AverageRate='NULL' THEN -1 ELSE CAST(AverageRate AS DECIMAL(8,2)) END AS AverageRate,
	CASE WHEN EndOfDayRate='NULL' THEN -1 ELSE CAST(EndOfDayRate AS DECIMAL(8,2)) END AS EndOfDayRate
FROM Dp_Stg1.Sales_May_06_10
UNION
SELECT DISTINCT 
	CASE WHEN CurrencyRateID1='NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID1,
	CASE WHEN CurrencyRateDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(CurrencyRateDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS CurrencyRateDate,
	CASE WHEN FromCurrencyCode = 'NULL' THEN '-1' ELSE FromCurrencyCode END AS FromCurrencyCode,
	CASE WHEN ToCurrencyCode = 'NULL' THEN '-1' ELSE ToCurrencyCode END AS ToCurrencyCode,
	CASE WHEN AverageRate='NULL' THEN -1 ELSE CAST(AverageRate AS DECIMAL(8,2)) END AS AverageRate,
	CASE WHEN EndOfDayRate='NULL' THEN -1 ELSE CAST(EndOfDayRate AS DECIMAL(8,2)) END AS EndOfDayRate
FROM Dp_Stg1.Sales_May_11_20
UNION
SELECT DISTINCT 
	CASE WHEN CurrencyRateID1='NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID1,
	CASE WHEN CurrencyRateDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(CurrencyRateDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS CurrencyRateDate,
	CASE WHEN FromCurrencyCode = 'NULL' THEN '-1' ELSE FromCurrencyCode END AS FromCurrencyCode,
	CASE WHEN ToCurrencyCode = 'NULL' THEN '-1' ELSE ToCurrencyCode END AS ToCurrencyCode,
	CASE WHEN AverageRate='NULL' THEN -1 ELSE CAST(AverageRate AS DECIMAL(8,2)) END AS AverageRate,
	CASE WHEN EndOfDayRate='NULL' THEN -1 ELSE CAST(EndOfDayRate AS DECIMAL(8,2)) END AS EndOfDayRate
FROM Dp_Stg1.Sales_May_21_31;

INSERT INTO DP_STG2.Customer
SELECT DISTINCT 
 CASE WHEN  CustomerID ='NULL' THEN -1 ELSE  CustomerID END AS  CustomerID,
    CASE WHEN  PersonID  = 'NULL' THEN -1 ELSE  PersonID END AS PersonID,
	CASE WHEN  StoreID  = 'NULL'  THEN -1 ELSE  StoreID END AS StoreID,
	CASE WHEN  TerritoryID = 'NULL' THEN -1 ELSE  TerritoryID END AS TerritoryID,
	CASE WHEN AccountNumber = 'NULL' THEN 'N/A' ELSE AccountNumber END AS AccountNumber
FROM  DP_STG1.Customer_Sales;

INSERT INTO DP_STG2.PersonCreditCard	
SELECT DISTINCT 
    CASE WHEN BusinessEntityID1='NULL' THEN -1 ELSE BusinessEntityID1 END AS  BusinessEntityID,
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID
FROM Dp_Stg1.Sales
UNION
SELECT DISTINCT 
	CASE WHEN BusinessEntityID1='NULL' THEN -1 ELSE BusinessEntityID1 END AS  BusinessEntityID,
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID
FROM Dp_Stg1.Sales_April_1_10
UNION
SELECT DISTINCT 
	CASE WHEN BusinessEntityID1='NULL' THEN -1 ELSE BusinessEntityID1 END AS  BusinessEntityID,
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID
FROM Dp_Stg1.Sales_April_11_20
UNION
SELECT DISTINCT 
	CASE WHEN BusinessEntityID1='NULL' THEN -1 ELSE BusinessEntityID1 END AS  BusinessEntityID,
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID
FROM Dp_Stg1.Sales_April_21_30
UNION
SELECT DISTINCT 
	CASE WHEN BusinessEntityID1='NULL' THEN -1 ELSE BusinessEntityID1 END AS  BusinessEntityID,
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID
FROM Dp_Stg1.Sales_May_06_10
UNION
SELECT DISTINCT 
	CASE WHEN BusinessEntityID1='NULL' THEN -1 ELSE BusinessEntityID1 END AS  BusinessEntityID,
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID
FROM Dp_Stg1.Sales_May_11_20
UNION
SELECT DISTINCT 
	CASE WHEN BusinessEntityID1='NULL' THEN -1 ELSE BusinessEntityID1 END AS  BusinessEntityID,
	CASE WHEN CreditCardID1='NULL' THEN -1 ELSE CreditCardID1 END AS  CreditCardID
FROM Dp_Stg1.Sales_May_21_31;

INSERT INTO DP_STG2.SalesOrderDetail
	SELECT DISTINCT
	CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
	CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
	CASE WHEN SalesOrderDetailID = 'NULL' THEN -1 ELSE SalesOrderDetailID END AS SalesOrderDetailID,
	CASE WHEN CarrierTrackingNumber = 'NULL' THEN 'N/A' ELSE CarrierTrackingNumber END AS CarrierTrackingNumber,
	CASE WHEN OrderQty = 'NULL' THEN -1 ELSE OrderQty END AS OrderQty,
	CASE WHEN ProductID1 = 'NULL' THEN -1 ELSE ProductID1 END AS ProductID,
	CASE WHEN SpecialOfferID1 = 'NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID,
	CASE WHEN  UnitPrice = 'NULL' THEN -1 ELSE  CAST(UnitPrice AS DECIMAL(10,2)) END AS UnitPrice,
	CASE WHEN UnitPriceDiscount = 'NULL' THEN -1 ELSE  CAST(UnitPriceDiscount AS DECIMAL(6,2)) END AS UnitPriceDiscount,
	CASE WHEN  LineTotal = 'NULL' THEN -1 ELSE  CAST(LineTotal AS DECIMAL(10,2)) END AS LineTotal
	FROM DP_STG1.Sales
	UNION
	SELECT DISTINCT
	CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
	CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
	CASE WHEN SalesOrderDetailID = 'NULL' THEN -1 ELSE SalesOrderDetailID END AS SalesOrderDetailID,
	CASE WHEN CarrierTrackingNumber = 'NULL' THEN 'N/A' ELSE CarrierTrackingNumber END AS CarrierTrackingNumber,
	CASE WHEN OrderQty = 'NULL' THEN -1 ELSE OrderQty END AS OrderQty,
	CASE WHEN ProductID1 = 'NULL' THEN -1 ELSE ProductID1 END AS ProductID,
	CASE WHEN SpecialOfferID1 = 'NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID,
	CASE WHEN  UnitPrice = 'NULL' THEN -1 ELSE  CAST(UnitPrice AS DECIMAL(10,2)) END AS UnitPrice,
	CASE WHEN UnitPriceDiscount = 'NULL' THEN -1 ELSE  CAST(UnitPriceDiscount AS DECIMAL(6,2)) END AS UnitPriceDiscount,
	CASE WHEN  LineTotal = 'NULL' THEN -1 ELSE  CAST(LineTotal AS DECIMAL(10,2)) END AS LineTotal
	FROM DP_STG1.Sales_April_1_10
	UNION
	SELECT DISTINCT
	CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
	CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
	CASE WHEN SalesOrderDetailID = 'NULL' THEN -1 ELSE SalesOrderDetailID END AS SalesOrderDetailID,
	CASE WHEN CarrierTrackingNumber = 'NULL' THEN 'N/A' ELSE CarrierTrackingNumber END AS CarrierTrackingNumber,
	CASE WHEN OrderQty = 'NULL' THEN -1 ELSE OrderQty END AS OrderQty,
	CASE WHEN ProductID1 = 'NULL' THEN -1 ELSE ProductID1 END AS ProductID,
	CASE WHEN SpecialOfferID1 = 'NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID,
	CASE WHEN  UnitPrice = 'NULL' THEN -1 ELSE  CAST(UnitPrice AS DECIMAL(10,2)) END AS UnitPrice,
	CASE WHEN UnitPriceDiscount = 'NULL' THEN -1 ELSE  CAST(UnitPriceDiscount AS DECIMAL(6,2)) END AS UnitPriceDiscount,
	CASE WHEN  LineTotal = 'NULL' THEN -1 ELSE  CAST(LineTotal AS DECIMAL(10,2)) END AS LineTotal
	FROM DP_STG1.Sales_April_11_20
	UNION
	SELECT DISTINCT
	CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
	CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
	CASE WHEN SalesOrderDetailID = 'NULL' THEN -1 ELSE SalesOrderDetailID END AS SalesOrderDetailID,
	CASE WHEN CarrierTrackingNumber = 'NULL' THEN 'N/A' ELSE CarrierTrackingNumber END AS CarrierTrackingNumber,
	CASE WHEN OrderQty = 'NULL' THEN -1 ELSE OrderQty END AS OrderQty,
	CASE WHEN ProductID1 = 'NULL' THEN -1 ELSE ProductID1 END AS ProductID,
	CASE WHEN SpecialOfferID1 = 'NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID,
	CASE WHEN  UnitPrice = 'NULL' THEN -1 ELSE  CAST(UnitPrice AS DECIMAL(10,2)) END AS UnitPrice,
	CASE WHEN UnitPriceDiscount = 'NULL' THEN -1 ELSE  CAST(UnitPriceDiscount AS DECIMAL(6,2)) END AS UnitPriceDiscount,
	CASE WHEN  LineTotal = 'NULL' THEN -1 ELSE  CAST(LineTotal AS DECIMAL(10,2)) END AS LineTotal
	FROM DP_STG1.Sales_April_21_30
	UNION
	SELECT DISTINCT
	CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
	CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
	CASE WHEN SalesOrderDetailID = 'NULL' THEN -1 ELSE SalesOrderDetailID END AS SalesOrderDetailID,
	CASE WHEN CarrierTrackingNumber = 'NULL' THEN 'N/A' ELSE CarrierTrackingNumber END AS CarrierTrackingNumber,
	CASE WHEN OrderQty = 'NULL' THEN -1 ELSE OrderQty END AS OrderQty,
	CASE WHEN ProductID1 = 'NULL' THEN -1 ELSE ProductID1 END AS ProductID,
	CASE WHEN SpecialOfferID1 = 'NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID,
	CASE WHEN  UnitPrice = 'NULL' THEN -1 ELSE  CAST(UnitPrice AS DECIMAL(10,2)) END AS UnitPrice,
	CASE WHEN UnitPriceDiscount = 'NULL' THEN -1 ELSE  CAST(UnitPriceDiscount AS DECIMAL(6,2)) END AS UnitPriceDiscount,
	CASE WHEN  LineTotal = 'NULL' THEN -1 ELSE  CAST(LineTotal AS DECIMAL(10,2)) END AS LineTotal
	FROM DP_STG1.Sales_May_06_10
	UNION
	SELECT DISTINCT
	CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
	CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
	CASE WHEN SalesOrderDetailID = 'NULL' THEN -1 ELSE SalesOrderDetailID END AS SalesOrderDetailID,
	CASE WHEN CarrierTrackingNumber = 'NULL' THEN 'N/A' ELSE CarrierTrackingNumber END AS CarrierTrackingNumber,
	CASE WHEN OrderQty = 'NULL' THEN -1 ELSE OrderQty END AS OrderQty,
	CASE WHEN ProductID1 = 'NULL' THEN -1 ELSE ProductID1 END AS ProductID,
	CASE WHEN SpecialOfferID1 = 'NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID,
	CASE WHEN  UnitPrice = 'NULL' THEN -1 ELSE  CAST(UnitPrice AS DECIMAL(10,2)) END AS UnitPrice,
	CASE WHEN UnitPriceDiscount = 'NULL' THEN -1 ELSE  CAST(UnitPriceDiscount AS DECIMAL(6,2)) END AS UnitPriceDiscount,
	CASE WHEN  LineTotal = 'NULL' THEN -1 ELSE  CAST(LineTotal AS DECIMAL(10,2)) END AS LineTotal
	FROM DP_STG1.Sales_May_11_20
	UNION
	SELECT DISTINCT
	CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
	CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
	CASE WHEN SalesOrderDetailID = 'NULL' THEN -1 ELSE SalesOrderDetailID END AS SalesOrderDetailID,
	CASE WHEN CarrierTrackingNumber = 'NULL' THEN 'N/A' ELSE CarrierTrackingNumber END AS CarrierTrackingNumber,
	CASE WHEN OrderQty = 'NULL' THEN -1 ELSE OrderQty END AS OrderQty,
	CASE WHEN ProductID1 = 'NULL' THEN -1 ELSE ProductID1 END AS ProductID,
	CASE WHEN SpecialOfferID1 = 'NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID,
	CASE WHEN  UnitPrice = 'NULL' THEN -1 ELSE  CAST(UnitPrice AS DECIMAL(10,2)) END AS UnitPrice,
	CASE WHEN UnitPriceDiscount = 'NULL' THEN -1 ELSE  CAST(UnitPriceDiscount AS DECIMAL(6,2)) END AS UnitPriceDiscount,
	CASE WHEN  LineTotal = 'NULL' THEN -1 ELSE  CAST(LineTotal AS DECIMAL(10,2)) END AS LineTotal
	FROM DP_STG1.Sales_May_21_31
	UNION
	SELECT DISTINCT
	CASE WHEN BusinessEntityID = 'NULL' THEN -1 ELSE BusinessEntityID END AS BusinessEntityID,
	CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
	CASE WHEN SalesOrderDetailID1 = 'NULL' THEN -1 ELSE SalesOrderDetailID1 END AS SalesOrderDetailID,
	CASE WHEN CarrierTrackingNumber1 = 'NULL' THEN 'N/A' ELSE CarrierTrackingNumber1 END AS CarrierTrackingNumber,
	CASE WHEN OrderQty1 = 'NULL' THEN -1 ELSE OrderQty1 END AS OrderQty,
	CASE WHEN ProductID1 = 'NULL' THEN -1 ELSE ProductID1 END AS ProductID,
	CASE WHEN SpecialOfferID1 = 'NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID,
	CASE WHEN  UnitPrice1 = 'NULL' THEN -1 ELSE  CAST(UnitPrice1 AS DECIMAL(10,2)) END AS UnitPrice,
	CASE WHEN UnitPriceDiscount1 = 'NULL' THEN -1 ELSE  CAST(UnitPriceDiscount1 AS DECIMAL(6,2)) END AS UnitPriceDiscount,
	CASE WHEN  LineTotal = 'NULL' THEN -1 ELSE  CAST(LineTotal AS DECIMAL(10,2)) END AS LineTotal
	FROM DP_STG1.Sales_3_New;
	
INSERT INTO DP_STG2.SalesOrderHeader
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
CASE WHEN ShipMethodID = 'NULL' THEN -1 ELSE ShipMethodID END AS ShipMethodID,
CASE WHEN RevisionNumber = 'NULL' THEN -1 ELSE RevisionNumber END AS RevisionNumber,
CASE WHEN OrderDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(OrderDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS OrderDate,
CASE WHEN DueDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(DueDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS DueDate,
CASE WHEN ShipDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(ShipDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS ShipDate,
CASE WHEN Status = 'NULL' THEN -1 ELSE CAST(Status AS BYTEINT) END AS Status,
CASE WHEN OnlineOrderFlag = 'NULL' THEN -1 ELSE CAST(OnlineOrderFlag AS BYTEINT) END AS OnlineOrderFlag,
CASE WHEN SalesOrderNumber IS NULL THEN 'N/A' ELSE SalesOrderNumber END AS SalesOrderNumber,
CASE WHEN PurchaseOrderNumber IS NULL THEN 'N/A' ELSE PurchaseOrderNumber END AS PurchaseOrderNumber,
CASE WHEN AccountNumber= 'NULL' THEN 'N/A' ELSE AccountNumber END AS AccountNumber,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN BillToAddressID = 'NULL' THEN -1 ELSE BillToAddressID END AS BillToAddressID,
CASE WHEN ShipToAddressID = 'NULL' THEN -1 ELSE ShipToAddressID END AS ShipToAddressID,
CASE WHEN CreditCardID1 = 'NULL' THEN -1 ELSE CreditCardID1 END AS CreditCardID1,
CASE WHEN CreditCardApprovalCode IS NULL THEN 'N/A' ELSE CreditCardApprovalCode END AS CreditCardApprovalCode,
CASE WHEN CurrencyRateID1 = 'NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID1,
CASE WHEN  SubTotal = 'NULL' THEN -1 ELSE  CAST(SubTotal AS DECIMAL(10,2)) END AS SubTotal,
CASE WHEN  TaxAmt = 'NULL' THEN -1 ELSE  CAST(TaxAmt AS DECIMAL(10,2)) END AS TaxAmt,
CASE WHEN  Freight = 'NULL' THEN -1 ELSE  CAST(Freight AS DECIMAL(10,2)) END AS Freight,
CASE WHEN  TotalDue = 'NULL' THEN -1 ELSE  CAST(TotalDue AS DECIMAL(10,2)) END AS TotalDue
FROM DP_STG1.Sales
UNION
--Sales_April_1_10
--INSERT INTO DP_STG2.SalesOrderHeader
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
CASE WHEN ShipMethodID = 'NULL' THEN -1 ELSE ShipMethodID END AS ShipMethodID,
CASE WHEN RevisionNumber = 'NULL' THEN -1 ELSE RevisionNumber END AS RevisionNumber,
CASE WHEN OrderDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(OrderDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS OrderDate,
CASE WHEN DueDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(DueDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS DueDate,
CASE WHEN ShipDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(ShipDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS ShipDate,
CASE WHEN Status = 'NULL' THEN -1 ELSE CAST(Status AS BYTEINT) END AS Status,
CASE WHEN OnlineOrderFlag = 'NULL' THEN -1 ELSE CAST(OnlineOrderFlag AS BYTEINT) END AS OnlineOrderFlag,
CASE WHEN SalesOrderNumber IS NULL THEN 'N/A' ELSE SalesOrderNumber END AS SalesOrderNumber,
CASE WHEN PurchaseOrderNumber IS NULL THEN 'N/A' ELSE PurchaseOrderNumber END AS PurchaseOrderNumber,
CASE WHEN AccountNumber= 'NULL' THEN 'N/A' ELSE AccountNumber END AS AccountNumber,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN BillToAddressID = 'NULL' THEN -1 ELSE BillToAddressID END AS BillToAddressID,
CASE WHEN ShipToAddressID = 'NULL' THEN -1 ELSE ShipToAddressID END AS ShipToAddressID,
CASE WHEN CreditCardID1 = 'NULL' THEN -1 ELSE CreditCardID1 END AS CreditCardID1,
CASE WHEN CreditCardApprovalCode IS NULL THEN 'N/A' ELSE CreditCardApprovalCode END AS CreditCardApprovalCode,
CASE WHEN CurrencyRateID1 = 'NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID1,
CASE WHEN  SubTotal = 'NULL' THEN -1 ELSE  CAST(SubTotal AS DECIMAL(10,2)) END AS SubTotal,
CASE WHEN  TaxAmt = 'NULL' THEN -1 ELSE  CAST(TaxAmt AS DECIMAL(10,2)) END AS TaxAmt,
CASE WHEN  Freight = 'NULL' THEN -1 ELSE  CAST(Freight AS DECIMAL(10,2)) END AS Freight,
CASE WHEN  TotalDue = 'NULL' THEN -1 ELSE  CAST(TotalDue AS DECIMAL(10,2)) END AS TotalDue
FROM DP_STG1.Sales_April_1_10
UNION
-- Sales_April_11_20
--INSERT INTO DP_STG2.SalesOrderHeader
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
CASE WHEN ShipMethodID = 'NULL' THEN -1 ELSE ShipMethodID END AS ShipMethodID,
CASE WHEN RevisionNumber = 'NULL' THEN -1 ELSE RevisionNumber END AS RevisionNumber,
CASE WHEN OrderDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(OrderDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS OrderDate,
CASE WHEN DueDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(DueDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS DueDate,
CASE WHEN ShipDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(ShipDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS ShipDate,
CASE WHEN Status = 'NULL' THEN -1 ELSE CAST(Status AS BYTEINT) END AS Status,
CASE WHEN OnlineOrderFlag = 'NULL' THEN -1 ELSE CAST(OnlineOrderFlag AS BYTEINT) END AS OnlineOrderFlag,
CASE WHEN SalesOrderNumber IS NULL THEN 'N/A' ELSE SalesOrderNumber END AS SalesOrderNumber,
CASE WHEN PurchaseOrderNumber IS NULL THEN 'N/A' ELSE PurchaseOrderNumber END AS PurchaseOrderNumber,
CASE WHEN AccountNumber= 'NULL' THEN 'N/A' ELSE AccountNumber END AS AccountNumber,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN BillToAddressID = 'NULL' THEN -1 ELSE BillToAddressID END AS BillToAddressID,
CASE WHEN ShipToAddressID = 'NULL' THEN -1 ELSE ShipToAddressID END AS ShipToAddressID,
CASE WHEN CreditCardID1 = 'NULL' THEN -1 ELSE CreditCardID1 END AS CreditCardID1,
CASE WHEN CreditCardApprovalCode IS NULL THEN 'N/A' ELSE CreditCardApprovalCode END AS CreditCardApprovalCode,
CASE WHEN CurrencyRateID1 = 'NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID,
CASE WHEN  SubTotal = 'NULL' THEN -1 ELSE  CAST(SubTotal AS DECIMAL(10,2)) END AS SubTotal,
CASE WHEN  TaxAmt = 'NULL' THEN -1 ELSE  CAST(TaxAmt AS DECIMAL(10,2)) END AS TaxAmt,
CASE WHEN  Freight = 'NULL' THEN -1 ELSE  CAST(Freight AS DECIMAL(10,2)) END AS Freight,
CASE WHEN  TotalDue = 'NULL' THEN -1 ELSE  CAST(TotalDue AS DECIMAL(10,2)) END AS TotalDue
FROM DP_STG1.Sales_April_11_20
UNION
--Sales_April_21_30

SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
CASE WHEN ShipMethodID = 'NULL' THEN -1 ELSE ShipMethodID END AS ShipMethodID,
CASE WHEN RevisionNumber = 'NULL' THEN -1 ELSE RevisionNumber END AS RevisionNumber,
CASE WHEN OrderDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(OrderDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS OrderDate,
CASE WHEN DueDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(DueDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS DueDate,
CASE WHEN ShipDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(ShipDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS ShipDate,
CASE WHEN Status = 'NULL' THEN -1 ELSE CAST(Status AS BYTEINT) END AS Status,
CASE WHEN OnlineOrderFlag = 'NULL' THEN -1 ELSE CAST(OnlineOrderFlag AS BYTEINT) END AS OnlineOrderFlag,
CASE WHEN SalesOrderNumber IS NULL THEN 'N/A' ELSE SalesOrderNumber END AS SalesOrderNumber,
CASE WHEN PurchaseOrderNumber IS NULL THEN 'N/A' ELSE PurchaseOrderNumber END AS PurchaseOrderNumber,
CASE WHEN AccountNumber= 'NULL' THEN 'N/A' ELSE AccountNumber END AS AccountNumber,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN BillToAddressID = 'NULL' THEN -1 ELSE BillToAddressID END AS BillToAddressID,
CASE WHEN ShipToAddressID = 'NULL' THEN -1 ELSE ShipToAddressID END AS ShipToAddressID,
CASE WHEN CreditCardID1 = 'NULL' THEN -1 ELSE CreditCardID1 END AS CreditCardID1,
CASE WHEN CreditCardApprovalCode IS NULL THEN 'N/A' ELSE CreditCardApprovalCode END AS CreditCardApprovalCode,
CASE WHEN CurrencyRateID1 = 'NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID,
CASE WHEN  SubTotal = 'NULL' THEN -1 ELSE  CAST(SubTotal AS DECIMAL(10,2)) END AS SubTotal,
CASE WHEN  TaxAmt = 'NULL' THEN -1 ELSE  CAST(TaxAmt AS DECIMAL(10,2)) END AS TaxAmt,
CASE WHEN  Freight = 'NULL' THEN -1 ELSE  CAST(Freight AS DECIMAL(10,2)) END AS Freight,
CASE WHEN  TotalDue = 'NULL' THEN -1 ELSE  CAST(TotalDue AS DECIMAL(10,2)) END AS TotalDue
FROM DP_STG1.Sales_April_21_30
UNION
--Sales_May_06_10
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
CASE WHEN ShipMethodID = 'NULL' THEN -1 ELSE ShipMethodID END AS ShipMethodID,
CASE WHEN RevisionNumber = 'NULL' THEN -1 ELSE RevisionNumber END AS RevisionNumber,
CASE WHEN OrderDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(OrderDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS OrderDate,
CASE WHEN DueDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(DueDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS DueDate,
CASE WHEN ShipDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(ShipDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS ShipDate,
CASE WHEN Status = 'NULL' THEN -1 ELSE CAST(Status AS BYTEINT) END AS Status,
CASE WHEN OnlineOrderFlag = 'NULL' THEN -1 ELSE CAST(OnlineOrderFlag AS BYTEINT) END AS OnlineOrderFlag,
CASE WHEN SalesOrderNumber IS NULL THEN 'N/A' ELSE SalesOrderNumber END AS SalesOrderNumber,
CASE WHEN PurchaseOrderNumber IS NULL THEN 'N/A' ELSE PurchaseOrderNumber END AS PurchaseOrderNumber,
CASE WHEN AccountNumber= 'NULL' THEN 'N/A' ELSE AccountNumber END AS AccountNumber,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN BillToAddressID = 'NULL' THEN -1 ELSE BillToAddressID END AS BillToAddressID,
CASE WHEN ShipToAddressID = 'NULL' THEN -1 ELSE ShipToAddressID END AS ShipToAddressID,
CASE WHEN CreditCardID1 = 'NULL' THEN -1 ELSE CreditCardID1 END AS CreditCardID1,
CASE WHEN CreditCardApprovalCode IS NULL THEN 'N/A' ELSE CreditCardApprovalCode END AS CreditCardApprovalCode,
CASE WHEN CurrencyRateID1 = 'NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID,
CASE WHEN  SubTotal = 'NULL' THEN -1 ELSE  CAST(SubTotal AS DECIMAL(10,2)) END AS SubTotal,
CASE WHEN  TaxAmt = 'NULL' THEN -1 ELSE  CAST(TaxAmt AS DECIMAL(10,2)) END AS TaxAmt,
CASE WHEN  Freight = 'NULL' THEN -1 ELSE  CAST(Freight AS DECIMAL(10,2)) END AS Freight,
CASE WHEN  TotalDue = 'NULL' THEN -1 ELSE  CAST(TotalDue AS DECIMAL(10,2)) END AS TotalDue
FROM DP_STG1.Sales_May_06_10
UNION
--Sales_May_11_20
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
CASE WHEN ShipMethodID = 'NULL' THEN -1 ELSE ShipMethodID END AS ShipMethodID,
CASE WHEN RevisionNumber = 'NULL' THEN -1 ELSE RevisionNumber END AS RevisionNumber,
CASE WHEN OrderDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(OrderDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS OrderDate,
CASE WHEN DueDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(DueDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS DueDate,
CASE WHEN ShipDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(ShipDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS ShipDate,
CASE WHEN Status = 'NULL' THEN -1 ELSE CAST(Status AS BYTEINT) END AS Status,
CASE WHEN OnlineOrderFlag = 'NULL' THEN -1 ELSE CAST(OnlineOrderFlag AS BYTEINT) END AS OnlineOrderFlag,
CASE WHEN SalesOrderNumber IS NULL THEN 'N/A' ELSE SalesOrderNumber END AS SalesOrderNumber,
CASE WHEN PurchaseOrderNumber IS NULL THEN 'N/A' ELSE PurchaseOrderNumber END AS PurchaseOrderNumber,
CASE WHEN AccountNumber= 'NULL' THEN 'N/A' ELSE AccountNumber END AS AccountNumber,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN BillToAddressID = 'NULL' THEN -1 ELSE BillToAddressID END AS BillToAddressID,
CASE WHEN ShipToAddressID = 'NULL' THEN -1 ELSE ShipToAddressID END AS ShipToAddressID,
CASE WHEN CreditCardID1 = 'NULL' THEN -1 ELSE CreditCardID1 END AS CreditCardID1,
CASE WHEN CreditCardApprovalCode IS NULL THEN 'N/A' ELSE CreditCardApprovalCode END AS CreditCardApprovalCode,
CASE WHEN CurrencyRateID1 = 'NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID,
CASE WHEN  SubTotal = 'NULL' THEN -1 ELSE  CAST(SubTotal AS DECIMAL(10,2)) END AS SubTotal,
CASE WHEN  TaxAmt = 'NULL' THEN -1 ELSE  CAST(TaxAmt AS DECIMAL(10,2)) END AS TaxAmt,
CASE WHEN  Freight = 'NULL' THEN -1 ELSE  CAST(Freight AS DECIMAL(10,2)) END AS Freight,
CASE WHEN  TotalDue = 'NULL' THEN -1 ELSE  CAST(TotalDue AS DECIMAL(10,2)) END AS TotalDue
FROM DP_STG1.Sales_May_11_20
UNION
--Sales_May_21_31
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
CASE WHEN ShipMethodID = 'NULL' THEN -1 ELSE ShipMethodID END AS ShipMethodID,
CASE WHEN RevisionNumber = 'NULL' THEN -1 ELSE RevisionNumber END AS RevisionNumber,
CASE WHEN OrderDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(OrderDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS OrderDate,
CASE WHEN DueDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(DueDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS DueDate,
CASE WHEN ShipDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(ShipDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS ShipDate,
CASE WHEN Status = 'NULL' THEN -1 ELSE CAST(Status AS BYTEINT) END AS Status,
CASE WHEN OnlineOrderFlag = 'NULL' THEN -1 ELSE CAST(OnlineOrderFlag AS BYTEINT) END AS OnlineOrderFlag,
CASE WHEN SalesOrderNumber IS NULL THEN 'N/A' ELSE SalesOrderNumber END AS SalesOrderNumber,
CASE WHEN PurchaseOrderNumber IS NULL THEN 'N/A' ELSE PurchaseOrderNumber END AS PurchaseOrderNumber,
CASE WHEN AccountNumber= 'NULL' THEN 'N/A' ELSE AccountNumber END AS AccountNumber,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN BillToAddressID = 'NULL' THEN -1 ELSE BillToAddressID END AS BillToAddressID,
CASE WHEN ShipToAddressID = 'NULL' THEN -1 ELSE ShipToAddressID END AS ShipToAddressID,
CASE WHEN CreditCardID1 = 'NULL' THEN -1 ELSE CreditCardID1 END AS CreditCardID1,
CASE WHEN CreditCardApprovalCode IS NULL THEN 'N/A' ELSE CreditCardApprovalCode END AS CreditCardApprovalCode,
CASE WHEN CurrencyRateID1 = 'NULL' THEN -1 ELSE CurrencyRateID1 END AS CurrencyRateID2,
CASE WHEN  SubTotal = 'NULL' THEN -1 ELSE  CAST(SubTotal AS DECIMAL(10,2)) END AS SubTotal,
CASE WHEN  TaxAmt = 'NULL' THEN -1 ELSE  CAST(TaxAmt AS DECIMAL(10,2)) END AS TaxAmt,
CASE WHEN  Freight = 'NULL' THEN -1 ELSE  CAST(Freight AS DECIMAL(10,2)) END AS Freight,
CASE WHEN  TotalDue = 'NULL' THEN -1 ELSE  CAST(TotalDue AS DECIMAL(10,2)) END AS TotalDue
FROM DP_STG1.Sales_May_21_31
UNION
SELECT DISTINCT
CASE WHEN BusinessEntityID = 'NULL' THEN -1 ELSE BusinessEntityID END AS BusinessEntityID,
CASE WHEN SalesOrderID1 = 'NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID1,
CASE WHEN ShipMethodID = 'NULL' THEN -1 ELSE ShipMethodID END AS ShipMethodID,
CASE WHEN RevisionNumber1 = 'NULL' THEN -1 ELSE RevisionNumber1 END AS RevisionNumber,
CASE WHEN OrderDate IS NULL THEN CAST('01/01/1900' AS DATE FORMAT 'MM/DD/YYYY') ELSE CAST(SUBSTR(OrderDate,1,10) AS DATE FORMAT 'MM/DD/YYYY') END AS OrderDate,
CASE WHEN DueDate = 'NULL' THEN CAST('01/01/1900' AS DATE FORMAT 'MM/DD/YYYY') ELSE CAST(SUBSTR(DueDate,1,10) AS DATE FORMAT 'MM/DD/YYYY') END AS DueDate,
CASE WHEN ShipDate = 'NULL' THEN CAST('01/01/1900' AS DATE FORMAT 'MM/DD/YYYY') ELSE CAST(SUBSTR(ShipDate,1,10) AS DATE FORMAT 'MM/DD/YYYY') END AS ShipDate,
CASE WHEN Status = 'NULL' THEN -1 ELSE CAST(Status AS BYTEINT) END AS Status,
CASE WHEN OnlineOrderFlag = 'NULL' THEN -1 ELSE CAST(OnlineOrderFlag AS BYTEINT) END AS OnlineOrderFlag,
CASE WHEN SalesOrderNumber1 IS NULL THEN 'N/A' ELSE SalesOrderNumber1 END AS SalesOrderNumber,
CASE WHEN PurchaseOrderNumber1 IS NULL THEN 'N/A' ELSE PurchaseOrderNumber1 END AS PurchaseOrderNumber,
CASE WHEN AccountNumber= 'NULL' THEN 'N/A' ELSE AccountNumber END AS AccountNumber,
CASE WHEN TerritoryID = 'NULL' THEN -1 ELSE TerritoryID END AS TerritoryID1,
CASE WHEN BillToAddressID = 'NULL' THEN -1 ELSE BillToAddressID END AS BillToAddressID,
CASE WHEN ShipToAddressID = 'NULL' THEN -1 ELSE ShipToAddressID END AS ShipToAddressID,
CASE WHEN CreditCardID = 'NULL' THEN -1 ELSE CreditCardID END AS CreditCardID,
CASE WHEN CreditCardApprovalCode IS NULL THEN 'N/A' ELSE CreditCardApprovalCode END AS CreditCardApprovalCode,
CASE WHEN CurrencyRateID = 'NULL' THEN -1 ELSE CurrencyRateID END AS CurrencyRateID2,
CASE WHEN  SubTotal = 'NULL' THEN -1 ELSE  CAST(SubTotal AS DECIMAL(10,2)) END AS SubTotal,
CASE WHEN  TaxAmt = 'NULL' THEN -1 ELSE  CAST(TaxAmt AS DECIMAL(10,2)) END AS TaxAmt,
CASE WHEN  Freight = 'NULL' THEN -1 ELSE  CAST(Freight AS DECIMAL(10,2)) END AS Freight,
CASE WHEN  TotalDue = 'NULL' THEN -1 ELSE  CAST(TotalDue AS DECIMAL(10,2)) END AS TotalDue
FROM DP_STG1.Sales_2;

INSERT INTO DP_STG2.SalesOrderHeaderSalesReason
SELECT DISTINCT 
    CASE WHEN BusinessEntityID1 ='NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
    CASE WHEN SalesOrderID1 ='NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID,
	CASE WHEN SalesReasonID1 ='NULL' THEN -1 ELSE SalesReasonID1 END AS SalesReasonID
FROM Dp_Stg1.Sales
UNION
SELECT DISTINCT 
	CASE WHEN BusinessEntityID1 ='NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
    CASE WHEN SalesOrderID1 ='NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID,
	CASE WHEN SalesReasonID1 ='NULL' THEN -1 ELSE SalesReasonID1 END AS SalesReasonID
FROM Dp_Stg1.Sales_April_1_10
UNION
SELECT DISTINCT 
	CASE WHEN BusinessEntityID1 ='NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
    CASE WHEN SalesOrderID1 ='NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID,
	CASE WHEN SalesReasonID1 ='NULL' THEN -1 ELSE SalesReasonID1 END AS SalesReasonID
FROM Dp_Stg1.Sales_April_11_20
UNION
SELECT DISTINCT 
	CASE WHEN BusinessEntityID1 ='NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
    CASE WHEN SalesOrderID1 ='NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID,
	CASE WHEN SalesReasonID1 ='NULL' THEN -1 ELSE SalesReasonID1 END AS SalesReasonID
FROM Dp_Stg1.Sales_April_21_30
UNION
SELECT DISTINCT 
	CASE WHEN BusinessEntityID1 ='NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
    CASE WHEN SalesOrderID1 ='NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID,
	CASE WHEN SalesReasonID1 ='NULL' THEN -1 ELSE SalesReasonID1 END AS SalesReasonID
FROM Dp_Stg1.Sales_May_06_10
UNION
SELECT DISTINCT 
	CASE WHEN BusinessEntityID1 ='NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
    CASE WHEN SalesOrderID1 ='NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID,
	CASE WHEN SalesReasonID1 ='NULL' THEN -1 ELSE SalesReasonID1 END AS SalesReasonID
FROM Dp_Stg1.Sales_May_11_20
UNION
SELECT DISTINCT 
	CASE WHEN BusinessEntityID1 ='NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
    CASE WHEN SalesOrderID1 ='NULL' THEN -1 ELSE SalesOrderID1 END AS SalesOrderID,
	CASE WHEN SalesReasonID1 ='NULL' THEN -1 ELSE SalesReasonID1 END AS SalesReasonID
FROM Dp_Stg1.Sales_May_21_31;

--Sales
INSERT INTO DP_STG2.SalesPerson
SELECT DISTINCT
CASE WHEN BusinessEntityID2 = 'NULL' THEN -1 ELSE BusinessEntityID2 END AS BusinessEntityID2,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN  SalesQuota2 = 'NULL' THEN -1 ELSE  CAST(SalesQuota2 AS DECIMAL(10,2)) END AS SalesQuota,
CASE WHEN  Bonus = 'NULL' THEN -1 ELSE  CAST(Bonus AS DECIMAL(10,2)) END AS Bonus,
CASE WHEN  CommissionPct = 'NULL' THEN -1 ELSE  CAST(CommissionPct AS DECIMAL(10,2)) END AS CommissionPct,
CASE WHEN  SalesYTD2 = 'NULL' THEN -1 ELSE  CAST(SalesYTD2 AS DECIMAL(10,2)) END AS SalesYTD,
CASE WHEN  SalesLastYear1 = 'NULL' THEN -1 ELSE  CAST(SalesLastYear1 AS DECIMAL(10,2)) END AS SalesLastYear
FROM DP_STG1.Sales
UNION
--Sales_April_1_10
--INSERT INTO DP_STG2.SalesPerson
SELECT DISTINCT
CASE WHEN BusinessEntityID2 = 'NULL' THEN -1 ELSE BusinessEntityID2 END AS BusinessEntityID2,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN  SalesQuota2 = 'NULL' THEN -1 ELSE  CAST(SalesQuota2 AS DECIMAL(10,2)) END AS SalesQuota2,
CASE WHEN  Bonus = 'NULL' THEN -1 ELSE  CAST(Bonus AS DECIMAL(10,2)) END AS Bonus,
CASE WHEN  CommissionPct = 'NULL' THEN -1 ELSE  CAST(CommissionPct AS DECIMAL(10,2)) END AS CommissionPct,
CASE WHEN  SalesYTD2 = 'NULL' THEN -1 ELSE  CAST(SalesYTD2 AS DECIMAL(10,2)) END AS SalesYTD,
CASE WHEN  SalesLastYear1 = 'NULL' THEN -1 ELSE  CAST(SalesLastYear1 AS DECIMAL(10,2)) END AS SalesLastYear
FROM DP_STG1.Sales_April_1_10
UNION
--Sales_April_11_20
--INSERT INTO DP_STG2.SalesPerson
SELECT DISTINCT
CASE WHEN BusinessEntityID2 = 'NULL' THEN -1 ELSE BusinessEntityID2 END AS BusinessEntityID2,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN  SalesQuota2 = 'NULL' THEN -1 ELSE  CAST(SalesQuota2 AS DECIMAL(10,2)) END AS SalesQuota2,
CASE WHEN  Bonus = 'NULL' THEN -1 ELSE  CAST(Bonus AS DECIMAL(10,2)) END AS Bonus,
CASE WHEN  CommissionPct = 'NULL' THEN -1 ELSE  CAST(CommissionPct AS DECIMAL(10,2)) END AS CommissionPct,
CASE WHEN  SalesYTD2 = 'NULL' THEN -1 ELSE  CAST(SalesYTD2 AS DECIMAL(10,2)) END AS SalesYTD,
CASE WHEN  SalesLastYear1 = 'NULL' THEN -1 ELSE  CAST(SalesLastYear1 AS DECIMAL(10,2)) END AS SalesLastYear
FROM DP_STG1.Sales_April_11_20
UNION
--Sales_April_21_30
--INSERT INTO DP_STG2.SalesPerson
SELECT DISTINCT
CASE WHEN BusinessEntityID2 = 'NULL' THEN -1 ELSE BusinessEntityID2 END AS BusinessEntityID2,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN  SalesQuota2 = 'NULL' THEN -1 ELSE  CAST(SalesQuota2 AS DECIMAL(10,2)) END AS SalesQuota2,
CASE WHEN  Bonus = 'NULL' THEN -1 ELSE  CAST(Bonus AS DECIMAL(10,2)) END AS Bonus,
CASE WHEN  CommissionPct = 'NULL' THEN -1 ELSE  CAST(CommissionPct AS DECIMAL(10,2)) END AS CommissionPct,
CASE WHEN  SalesYTD2 = 'NULL' THEN -1 ELSE  CAST(SalesYTD2 AS DECIMAL(10,2)) END AS SalesYTD,
CASE WHEN  SalesLastYear1 = 'NULL' THEN -1 ELSE  CAST(SalesLastYear1 AS DECIMAL(10,2)) END AS SalesLastYear
FROM DP_STG1.Sales_April_21_30
UNION
--Sales_May_06_10
--INSERT INTO DP_STG2.SalesPerson
SELECT DISTINCT
CASE WHEN BusinessEntityID2 = 'NULL' THEN -1 ELSE BusinessEntityID2 END AS BusinessEntityID2,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN  SalesQuota2 = 'NULL' THEN -1 ELSE  CAST(SalesQuota2 AS DECIMAL(10,2)) END AS SalesQuota,
CASE WHEN  Bonus = 'NULL' THEN -1 ELSE  CAST(Bonus AS DECIMAL(10,2)) END AS Bonus,
CASE WHEN  CommissionPct = 'NULL' THEN -1 ELSE  CAST(CommissionPct AS DECIMAL(10,2)) END AS CommissionPct,
CASE WHEN  SalesYTD2 = 'NULL' THEN -1 ELSE  CAST(SalesYTD2 AS DECIMAL(10,2)) END AS SalesYTD,
CASE WHEN  SalesLastYear1 = 'NULL' THEN -1 ELSE  CAST(SalesLastYear1 AS DECIMAL(10,2)) END AS SalesLastYear
FROM DP_STG1.Sales_May_06_10
UNION
--Sales_May_11_20
--INSERT INTO DP_STG2.SalesPerson
SELECT DISTINCT
CASE WHEN BusinessEntityID2 = 'NULL' THEN -1 ELSE BusinessEntityID2 END AS BusinessEntityID2,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN  SalesQuota2 = 'NULL' THEN -1 ELSE  CAST(SalesQuota2 AS DECIMAL(10,2)) END AS SalesQuota,
CASE WHEN  Bonus = 'NULL' THEN -1 ELSE  CAST(Bonus AS DECIMAL(10,2)) END AS Bonus,
CASE WHEN  CommissionPct = 'NULL' THEN -1 ELSE  CAST(CommissionPct AS DECIMAL(10,2)) END AS CommissionPct,
CASE WHEN  SalesYTD2 = 'NULL' THEN -1 ELSE  CAST(SalesYTD2 AS DECIMAL(10,2)) END AS SalesYTD,
CASE WHEN  SalesLastYear1 = 'NULL' THEN -1 ELSE  CAST(SalesLastYear1 AS DECIMAL(10,2)) END AS SalesLastYear
FROM DP_STG1.Sales_May_11_20
UNION
--Sales_May_21_31
--INSERT INTO DP_STG2.SalesPerson
SELECT DISTINCT
CASE WHEN BusinessEntityID2 = 'NULL' THEN -1 ELSE BusinessEntityID2 END AS BusinessEntityID2,
CASE WHEN TerritoryID1 = 'NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID1,
CASE WHEN  SalesQuota2 = 'NULL' THEN -1 ELSE  CAST(SalesQuota2 AS DECIMAL(10,2)) END AS SalesQuota,
CASE WHEN  Bonus = 'NULL' THEN -1 ELSE  CAST(Bonus AS DECIMAL(10,2)) END AS Bonus,
CASE WHEN  CommissionPct = 'NULL' THEN -1 ELSE  CAST(CommissionPct AS DECIMAL(10,2)) END AS CommissionPct,
CASE WHEN  SalesYTD2 = 'NULL' THEN -1 ELSE  CAST(SalesYTD2 AS DECIMAL(10,2)) END AS SalesYTD,
CASE WHEN  SalesLastYear1 = 'NULL' THEN -1 ELSE  CAST(SalesLastYear1 AS DECIMAL(10,2)) END AS SalesLastYear
FROM DP_STG1.Sales_May_21_31;


INSERT INTO DP_STG2.SalesPersonQuotaHistory
SELECT DISTINCT 
    CASE WHEN  BusinessEntityID2 ='NULL' THEN -1 ELSE  BusinessEntityID2 END AS  BusinessEntityID,
    CASE WHEN  SalesQuota1 = 'NULL' THEN -1 ELSE  CAST(SalesQuota1 AS DECIMAL(10,2)) END AS SalesQuota,
	CASE WHEN QuotaDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(QuotaDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS QuotaDate
FROM Dp_Stg1.Sales
UNION
SELECT DISTINCT 
	CASE WHEN  BusinessEntityID2 ='NULL' THEN -1 ELSE  BusinessEntityID2 END AS  BusinessEntityID,
    CASE WHEN  SalesQuota1 = 'NULL' THEN -1 ELSE  CAST(SalesQuota1 AS DECIMAL(10,2)) END AS SalesQuota,
	CASE WHEN QuotaDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(QuotaDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS QuotaDate
FROM Dp_Stg1.Sales_April_1_10
UNION
SELECT DISTINCT 
	CASE WHEN  BusinessEntityID2 ='NULL' THEN -1 ELSE  BusinessEntityID2 END AS  BusinessEntityID,
    CASE WHEN  SalesQuota1 = 'NULL' THEN -1 ELSE  CAST(SalesQuota1 AS DECIMAL(10,2)) END AS SalesQuota,
	CASE WHEN QuotaDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(QuotaDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS QuotaDate
FROM Dp_Stg1.Sales_April_11_20
UNION
SELECT DISTINCT 
CASE WHEN  BusinessEntityID2 ='NULL' THEN -1 ELSE  BusinessEntityID2 END AS  BusinessEntityID,
    CASE WHEN  SalesQuota1 = 'NULL' THEN -1 ELSE  CAST(SalesQuota1 AS DECIMAL(10,2)) END AS SalesQuota,
	CASE WHEN QuotaDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(QuotaDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS QuotaDate
FROM Dp_Stg1.Sales_April_21_30
UNION
SELECT DISTINCT 
	CASE WHEN  BusinessEntityID2 ='NULL' THEN -1 ELSE  BusinessEntityID2 END AS  BusinessEntityID,
    CASE WHEN  SalesQuota1 = 'NULL' THEN -1 ELSE  CAST(SalesQuota1 AS DECIMAL(10,2)) END AS SalesQuota,
	CASE WHEN QuotaDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(QuotaDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS QuotaDate
FROM Dp_Stg1.Sales_May_06_10
UNION
SELECT DISTINCT 
	CASE WHEN  BusinessEntityID2 ='NULL' THEN -1 ELSE  BusinessEntityID2 END AS  BusinessEntityID,
    CASE WHEN  SalesQuota1 = 'NULL' THEN -1 ELSE  CAST(SalesQuota1 AS DECIMAL(10,2)) END AS SalesQuota,
	CASE WHEN QuotaDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(QuotaDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS QuotaDate
FROM Dp_Stg1.Sales_May_11_20
UNION
SELECT DISTINCT 
	CASE WHEN  BusinessEntityID2 ='NULL' THEN -1 ELSE  BusinessEntityID2 END AS  BusinessEntityID,
    CASE WHEN  SalesQuota1 = 'NULL' THEN -1 ELSE  CAST(SalesQuota1 AS DECIMAL(10,2)) END AS SalesQuota,
	CASE WHEN QuotaDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(QuotaDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS QuotaDate
FROM Dp_Stg1.Sales_May_21_31;

INSERT INTO DP_STG2.SalesReason
SELECT DISTINCT 
	CASE WHEN SalesReasonID1 ='NULL' THEN -1 ELSE SalesReasonID1 END AS SalesReasonID,
	CASE WHEN Name1 = 'NULL' THEN 'N/A' ELSE Name1 END AS Name1,
	CASE WHEN ReasonType = 'NULL' THEN 'N/A' ELSE ReasonType END AS ReasonType
FROM Dp_Stg1.Sales;

INSERT INTO DP_STG2.SalesTerritory
SELECT DISTINCT 
    CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN Name3 ='NULL' THEN 'N/A' ELSE Name3 END AS Name3,
	CASE WHEN  CountryRegionCode ='NULL' THEN 'N/A' ELSE  CountryRegionCode END AS  CountryRegionCode,
	CASE WHEN Group1 ='NULL' THEN 'N/A' ELSE Group1 END AS Group1,
	CASE WHEN SalesYtd1 ='NULL' THEN -1 ELSE CAST(SalesYtd1 AS DECIMAL(15,2)) END AS SalesYtd,
	CASE WHEN SalesLastYear1 ='NULL' THEN -1 ELSE CAST(SalesLastYear1 AS DECIMAL(15,2)) END AS SalesLastYear,
	CASE WHEN CostYtd ='NULL' THEN -1 ELSE CAST(CostYtd AS DECIMAL(15,2)) END AS CostYtd,
	CASE WHEN CostLastYear ='NULL' THEN -1 ELSE CAST(CostLastYear AS DECIMAL(15,2)) END AS CostLastYear
FROM Dp_Stg1.Sales
UNION
SELECT DISTINCT 
	CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN Name3 ='NULL' THEN 'N/A' ELSE Name3 END AS Name1,
	CASE WHEN  CountryRegionCode ='NULL' THEN 'N/A' ELSE  CountryRegionCode END AS  CountryRegionCode,
	CASE WHEN Group1 ='NULL' THEN 'N/A' ELSE Group1 END AS Group1,
	CASE WHEN SalesYtd1 ='NULL' THEN -1 ELSE CAST(SalesYtd1 AS DECIMAL(15,2)) END AS SalesYtd,
	CASE WHEN SalesLastYear1 ='NULL' THEN -1 ELSE CAST(SalesLastYear1 AS DECIMAL(15,2)) END AS SalesLastYear,
	CASE WHEN CostYtd ='NULL' THEN -1 ELSE CAST(CostYtd AS DECIMAL(15,2)) END AS CostYtd,
	CASE WHEN CostLastYear ='NULL' THEN -1 ELSE CAST(CostLastYear AS DECIMAL(15,2)) END AS CostLastYear
FROM Dp_Stg1.Sales_April_1_10
UNION
SELECT DISTINCT 
	CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN Name3 ='NULL' THEN 'N/A' ELSE Name3 END AS Name1,
	CASE WHEN  CountryRegionCode ='NULL' THEN 'N/A' ELSE  CountryRegionCode END AS  CountryRegionCode,
	CASE WHEN Group1 ='NULL' THEN 'N/A' ELSE Group1 END AS Group1,
	CASE WHEN SalesYtd1 ='NULL' THEN -1 ELSE CAST(SalesYtd1 AS DECIMAL(15,2)) END AS SalesYtd,
	CASE WHEN SalesLastYear1 ='NULL' THEN -1 ELSE CAST(SalesLastYear1 AS DECIMAL(15,2)) END AS SalesLastYear,
	CASE WHEN CostYtd ='NULL' THEN -1 ELSE CAST(CostYtd AS DECIMAL(15,2)) END AS CostYtd,
	CASE WHEN CostLastYear ='NULL' THEN -1 ELSE CAST(CostLastYear AS DECIMAL(15,2)) END AS CostLastYear
FROM Dp_Stg1.Sales_April_11_20
UNION
SELECT DISTINCT 
	CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN Name3 ='NULL' THEN 'N/A' ELSE Name3 END AS Name1,
	CASE WHEN  CountryRegionCode ='NULL' THEN 'N/A' ELSE  CountryRegionCode END AS  CountryRegionCode,
	CASE WHEN Group1 ='NULL' THEN 'N/A' ELSE Group1 END AS Group1,
	CASE WHEN SalesYtd1 ='NULL' THEN -1 ELSE CAST(SalesYtd1 AS DECIMAL(15,2)) END AS SalesYtd,
	CASE WHEN SalesLastYear1 ='NULL' THEN -1 ELSE CAST(SalesLastYear1 AS DECIMAL(15,2)) END AS SalesLastYear,
	CASE WHEN CostYtd ='NULL' THEN -1 ELSE CAST(CostYtd AS DECIMAL(15,2)) END AS CostYtd,
	CASE WHEN CostLastYear ='NULL' THEN -1 ELSE CAST(CostLastYear AS DECIMAL(15,2)) END AS CostLastYear
FROM Dp_Stg1.Sales_April_21_30
UNION
SELECT DISTINCT 
	CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN Name3 ='NULL' THEN 'N/A' ELSE Name3 END AS Name1,
	CASE WHEN  CountryRegionCode ='NULL' THEN 'N/A' ELSE  CountryRegionCode END AS  CountryRegionCode,
	CASE WHEN Group1 ='NULL' THEN 'N/A' ELSE Group1 END AS Group1,
	CASE WHEN SalesYtd1 ='NULL' THEN -1 ELSE CAST(SalesYtd1 AS DECIMAL(15,2)) END AS SalesYtd,
	CASE WHEN SalesLastYear1 ='NULL' THEN -1 ELSE CAST(SalesLastYear1 AS DECIMAL(15,2)) END AS SalesLastYear,
	CASE WHEN CostYtd ='NULL' THEN -1 ELSE CAST(CostYtd AS DECIMAL(15,2)) END AS CostYtd,
	CASE WHEN CostLastYear ='NULL' THEN -1 ELSE CAST(CostLastYear AS DECIMAL(15,2)) END AS CostLastYear
FROM Dp_Stg1.Sales_May_06_10
UNION
SELECT DISTINCT 
	CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN Name3 ='NULL' THEN 'N/A' ELSE Name3 END AS Name1,
	CASE WHEN  CountryRegionCode ='NULL' THEN 'N/A' ELSE  CountryRegionCode END AS  CountryRegionCode,
	CASE WHEN Group1 ='NULL' THEN 'N/A' ELSE Group1 END AS Group1,
	CASE WHEN SalesYtd1 ='NULL' THEN -1 ELSE CAST(SalesYtd1 AS DECIMAL(15,2)) END AS SalesYtd,
	CASE WHEN SalesLastYear1 ='NULL' THEN -1 ELSE CAST(SalesLastYear1 AS DECIMAL(15,2)) END AS SalesLastYear,
	CASE WHEN CostYtd ='NULL' THEN -1 ELSE CAST(CostYtd AS DECIMAL(15,2)) END AS CostYtd,
	CASE WHEN CostLastYear ='NULL' THEN -1 ELSE CAST(CostLastYear AS DECIMAL(15,2)) END AS CostLastYear
FROM Dp_Stg1.Sales_May_11_20
UNION
SELECT DISTINCT 
	CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN Name3 ='NULL' THEN 'N/A' ELSE Name3 END AS Name1,
	CASE WHEN  CountryRegionCode ='NULL' THEN 'N/A' ELSE  CountryRegionCode END AS  CountryRegionCode,
	CASE WHEN Group1 ='NULL' THEN 'N/A' ELSE Group1 END AS Group1,
	CASE WHEN SalesYtd1 ='NULL' THEN -1 ELSE CAST(SalesYtd1 AS DECIMAL(15,2)) END AS SalesYtd,
	CASE WHEN SalesLastYear1 ='NULL' THEN -1 ELSE CAST(SalesLastYear1 AS DECIMAL(15,2)) END AS SalesLastYear,
	CASE WHEN CostYtd ='NULL' THEN -1 ELSE CAST(CostYtd AS DECIMAL(15,2)) END AS CostYtd,
	CASE WHEN CostLastYear ='NULL' THEN -1 ELSE CAST(CostLastYear AS DECIMAL(15,2)) END AS CostLastYear
FROM Dp_Stg1.Sales_May_21_31;

INSERT INTO DP_STG2.SalesTaxRate
SELECT DISTINCT 
	CASE WHEN SalesTaxRateID = 'NULL' THEN -1 ELSE SalesTaxRateID END AS  SalesTaxRateID,
	CASE WHEN StateProvinceID = 'NULL' THEN -1 ELSE StateProvinceID END AS  StateProvinceID,
	CASE WHEN TaxType = 'NULL' THEN -1 ELSE CAST(TaxType AS BYTEINT) END AS  TaxType,
	CASE WHEN TaxRate = 'NULL' THEN -1 ELSE CAST(TaxRate AS DECIMAL(4,2)) END AS  TaxRate,
	CASE WHEN "Name" ='NULL' THEN 'N/A' ELSE "Name" END  AS "Name"
FROM Dp_Stg1.SalesTaxRate_Sales;

INSERT INTO DP_STG2.SalesTerritoryHistory
SELECT DISTINCT 
    CASE WHEN  BusinessEntityID1 ='NULL' THEN -1 ELSE  BusinessEntityID1 END AS  BusinessEntityID,
    CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate
FROM Dp_Stg1.Sales
UNION
SELECT DISTINCT 
	CASE WHEN  BusinessEntityID1 ='NULL' THEN -1 ELSE  BusinessEntityID1 END AS  BusinessEntityID,
    CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate
FROM Dp_Stg1.Sales_April_1_10
UNION
SELECT DISTINCT 
	CASE WHEN  BusinessEntityID1 ='NULL' THEN -1 ELSE  BusinessEntityID1 END AS  BusinessEntityID,
    CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate
FROM Dp_Stg1.Sales_April_11_20
UNION
SELECT DISTINCT 
CASE WHEN  BusinessEntityID1 ='NULL' THEN -1 ELSE  BusinessEntityID1 END AS  BusinessEntityID,
    CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate
FROM Dp_Stg1.Sales_April_21_30
UNION
SELECT DISTINCT 
	CASE WHEN  BusinessEntityID1 ='NULL' THEN -1 ELSE  BusinessEntityID1 END AS  BusinessEntityID,
    CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate
FROM Dp_Stg1.Sales_May_06_10
UNION
SELECT DISTINCT 
	CASE WHEN  BusinessEntityID1 ='NULL' THEN -1 ELSE  BusinessEntityID1 END AS  BusinessEntityID,
    CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate
FROM Dp_Stg1.Sales_May_11_20
UNION
SELECT DISTINCT 
	CASE WHEN  BusinessEntityID1 ='NULL' THEN -1 ELSE  BusinessEntityID1 END AS  BusinessEntityID,
    CASE WHEN TerritoryID1 ='NULL' THEN -1 ELSE TerritoryID1 END AS TerritoryID,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate
FROM Dp_Stg1.Sales_May_21_31;

INSERT INTO DP_STG2.ShoppingCartItem
SELECT DISTINCT 
	CASE WHEN ShoppingCartItemID = 'NULL' THEN -1 ELSE ShoppingCartItemID END AS  ShoppingCartItemID,
	CASE WHEN ShoppingCartID = 'NULL' THEN -1 ELSE ShoppingCartID END AS ShoppingCartID,
	CASE WHEN Quantity = 'NULL' THEN -1 ELSE Quantity END AS  Quantity,
	CASE WHEN ProductID = 'NULL' THEN -1 ELSE ProductID END AS  ProductID,
	CASE WHEN DateCreated = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(DateCreated,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS DateCreated
FROM Dp_Stg1.ShoppingCartItem_Sales;

INSERT INTO DP_STG2.SpecialOffer
SELECT DISTINCT 
    CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
	CASE WHEN Description ='NULL' THEN 'N/A' ELSE Description END AS Description,
	CASE WHEN DiscountPct ='NULL' THEN -1 ELSE CAST(DiscountPct AS DECIMAL(8,2)) END AS DiscountPct,
	CASE WHEN Type1 ='NULL' THEN 'N/A' ELSE Type1 END AS Type1,
	CASE WHEN Category ='NULL' THEN 'N/A' ELSE Category END AS Category,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate,
    CASE WHEN MinQty ='NULL' THEN -1 ELSE MinQty END AS MinQty,
	CASE WHEN MaxQty ='NULL' THEN -1 ELSE MaxQty END AS MaxQty
FROM Dp_Stg1.Sales
UNION
SELECT DISTINCT 
	CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
	CASE WHEN Description ='NULL' THEN 'N/A' ELSE Description END AS Description,
	CASE WHEN DiscountPct ='NULL' THEN -1 ELSE CAST(DiscountPct AS DECIMAL(8,2)) END AS DiscountPct,
	CASE WHEN Type1 ='NULL' THEN 'N/A' ELSE Type1 END AS Type1,
	CASE WHEN Category ='NULL' THEN 'N/A' ELSE Category END AS Category,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate,
    CASE WHEN MinQty ='NULL' THEN -1 ELSE MinQty END AS MinQty,
	CASE WHEN MaxQty ='NULL' THEN -1 ELSE MaxQty END AS MaxQty
FROM Dp_Stg1.Sales_April_1_10
UNION
SELECT DISTINCT 
	CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
	CASE WHEN Description ='NULL' THEN 'N/A' ELSE Description END AS Description,
	CASE WHEN DiscountPct ='NULL' THEN -1 ELSE CAST(DiscountPct AS DECIMAL(8,2)) END AS DiscountPct,
	CASE WHEN Type1 ='NULL' THEN 'N/A' ELSE Type1 END AS Type1,
	CASE WHEN Category ='NULL' THEN 'N/A' ELSE Category END AS Category,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate,
    CASE WHEN MinQty ='NULL' THEN -1 ELSE MinQty END AS MinQty,
	CASE WHEN MaxQty ='NULL' THEN -1 ELSE MaxQty END AS MaxQty
FROM Dp_Stg1.Sales_April_11_20
UNION
SELECT DISTINCT 
	CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
	CASE WHEN Description ='NULL' THEN 'N/A' ELSE Description END AS Description,
	CASE WHEN DiscountPct ='NULL' THEN -1 ELSE CAST(DiscountPct AS DECIMAL(8,2)) END AS DiscountPct,
	CASE WHEN Type1 ='NULL' THEN 'N/A' ELSE Type1 END AS Type1,
	CASE WHEN Category ='NULL' THEN 'N/A' ELSE Category END AS Category,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate,
    CASE WHEN MinQty ='NULL' THEN -1 ELSE MinQty END AS MinQty,
	CASE WHEN MaxQty ='NULL' THEN -1 ELSE MaxQty END AS MaxQty
FROM Dp_Stg1.Sales_April_21_30
UNION
SELECT DISTINCT 
	CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
	CASE WHEN Description ='NULL' THEN 'N/A' ELSE Description END AS Description,
	CASE WHEN DiscountPct ='NULL' THEN -1 ELSE CAST(DiscountPct AS DECIMAL(8,2)) END AS DiscountPct,
	CASE WHEN Type1 ='NULL' THEN 'N/A' ELSE Type1 END AS Type1,
	CASE WHEN Category ='NULL' THEN 'N/A' ELSE Category END AS Category,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate,
    CASE WHEN MinQty ='NULL' THEN -1 ELSE MinQty END AS MinQty,
	CASE WHEN MaxQty ='NULL' THEN -1 ELSE MaxQty END AS MaxQty
FROM Dp_Stg1.Sales_May_06_10
UNION
SELECT DISTINCT 
	CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
	CASE WHEN Description ='NULL' THEN 'N/A' ELSE Description END AS Description,
	CASE WHEN DiscountPct ='NULL' THEN -1 ELSE CAST(DiscountPct AS DECIMAL(8,2)) END AS DiscountPct,
	CASE WHEN Type1 ='NULL' THEN 'N/A' ELSE Type1 END AS Type1,
	CASE WHEN Category ='NULL' THEN 'N/A' ELSE Category END AS Category,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate,
    CASE WHEN MinQty ='NULL' THEN -1 ELSE MinQty END AS MinQty,
	CASE WHEN MaxQty ='NULL' THEN -1 ELSE MaxQty END AS MaxQty
FROM Dp_Stg1.Sales_May_11_20
UNION
SELECT DISTINCT 
	CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
	CASE WHEN Description ='NULL' THEN 'N/A' ELSE Description END AS Description,
	CASE WHEN DiscountPct ='NULL' THEN -1 ELSE CAST(DiscountPct AS DECIMAL(8,2)) END AS DiscountPct,
	CASE WHEN Type1 ='NULL' THEN 'N/A' ELSE Type1 END AS Type1,
	CASE WHEN Category ='NULL' THEN 'N/A' ELSE Category END AS Category,
	CASE WHEN StartDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(StartDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
	CASE WHEN EndDate1 = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(EndDate1,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate,
    CASE WHEN MinQty ='NULL' THEN -1 ELSE MinQty END AS MinQty,
	CASE WHEN MaxQty ='NULL' THEN -1 ELSE MaxQty END AS MaxQty
FROM Dp_Stg1.Sales_May_21_31;

INSERT INTO DP_STG2.SpecialOfferProduct
SELECT DISTINCT 
    CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
    CASE WHEN ProductID1 ='NULL' THEN -1 ELSE ProductID1 END AS ProductID1
FROM Dp_Stg1.Sales
UNION
SELECT DISTINCT 
	CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
    CASE WHEN ProductID1 ='NULL' THEN -1 ELSE ProductID1 END AS ProductID1
FROM Dp_Stg1.Sales_April_1_10
UNION
SELECT DISTINCT 
	CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
    CASE WHEN ProductID1 ='NULL' THEN -1 ELSE ProductID1 END AS ProductID1
FROM Dp_Stg1.Sales_April_11_20
UNION
SELECT DISTINCT 
	CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
    CASE WHEN ProductID1 ='NULL' THEN -1 ELSE ProductID1 END AS ProductID1
FROM Dp_Stg1.Sales_April_21_30
UNION
SELECT DISTINCT 
	CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
    CASE WHEN ProductID1 ='NULL' THEN -1 ELSE ProductID1 END AS ProductID1
FROM Dp_Stg1.Sales_May_06_10
UNION
SELECT DISTINCT 
	CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
    CASE WHEN ProductID1 ='NULL' THEN -1 ELSE ProductID1 END AS ProductID1
FROM Dp_Stg1.Sales_May_11_20
UNION
SELECT DISTINCT 
	CASE WHEN SpecialOfferID1 ='NULL' THEN -1 ELSE SpecialOfferID1 END AS SpecialOfferID1,
    CASE WHEN ProductID1 ='NULL' THEN -1 ELSE ProductID1 END AS ProductID1
FROM Dp_Stg1.Sales_May_21_31;

INSERT INTO DP_STG2.Store	
SELECT DISTINCT 
	CASE WHEN BusinessEntityID='NULL' THEN -1 ELSE BusinessEntityID END AS  BusinessEntityID,
	CASE WHEN SalesPersonID='NULL' THEN -1 ELSE SalesPersonID END AS  SalesPersonID,
	CASE WHEN Name1='NULL' THEN 'N/A' ELSE Name1 END  AS Name1,
	CASE WHEN DEMOGRAPHICS = 'NULL' THEN 'N/A' ELSE DEMOGRAPHICS END AS DEMOGRAPHICS
FROM Dp_Stg1.Store_Sales;

