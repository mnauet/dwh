
--Person Module
  

INSERT INTO DP_STG2.AddressType
SELECT DISTINCT
CASE WHEN AddressTypeID1 = 'NULL' THEN -1 ELSE AddressTypeID1 END AS AddressTypeID,
CASE WHEN Name4 = 'NULL' THEN 'N/A' ELSE Name4 END AS Name4
FROM DP_STG1.Person;

INSERT INTO DP_STG2.BusinessEntity
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1
FROM DP_STG1.Person;

INSERT INTO DP_STG2.BusinessEntityAddress
SELECT DISTINCT
CASE WHEN AddressID1 = 'NULL' THEN -1 ELSE AddressID1 END AS AddressID1,
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN AddressTypeID1 = 'NULL' THEN -1 ELSE AddressTypeID1 END AS AddressTypeID
FROM DP_STG1.Person;

INSERT INTO DP_STG2.ContactType
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN Name1 = 'NULL' THEN 'N/A' ELSE Name1 END AS Name1
FROM DP_STG1.Person;

INSERT INTO DP_STG2.CountryRegion
SELECT DISTINCT
CASE WHEN CountryRegionCode1 = 'NULL' THEN 'N/A' ELSE CAST(CountryRegionCode1 AS CHAR(4)) END AS CountryRegionCode,
CASE WHEN Name3 = 'NULL' THEN 'N/A' ELSE Name3 END AS Name3
FROM DP_STG1.Person;

INSERT INTO DP_STG2.EmailAddress
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN EmailAddressID = 'NULL' THEN -1 ELSE EmailAddressID END AS EmailAddressID,
CASE WHEN EmailAddress = 'NULL' THEN 'N/A' ELSE EmailAddress END AS EmailAddress
FROM DP_STG1.Person;

INSERT INTO DP_STG2.Passwords
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN PasswordHash = 'NULL' THEN 'N/A' ELSE PasswordHash END AS PasswordHash,
CASE WHEN PasswordSalt = 'NULL' THEN 'N/A' ELSE PasswordSalt END AS PasswordSalt
FROM DP_STG1.Person;

INSERT INTO DP_STG2.Person
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN PersonType = 'NULL' THEN 'N/A' ELSE PersonType END AS PersonType,
CASE WHEN Title1 = 'NULL' THEN 'N/A' ELSE CAST(Title1 AS CHAR(4)) END AS Title1,
CASE WHEN FirstName = 'NULL' THEN 'N/A' ELSE FirstName END AS FirstName,
CASE WHEN MiddleName = 'NULL' THEN 'N/A' ELSE MiddleName END AS MiddleName,
CASE WHEN LastName = 'NULL' THEN 'N/A' ELSE LastName END AS LastName,
CASE WHEN Suffix = 'NULL' THEN 'N/A' ELSE CAST(Suffix AS CHAR(5)) END AS Suffix,
CASE WHEN EmailPromotion = 'NULL' THEN -1 ELSE CAST(EmailPromotion AS BYTEINT) END AS EmailPromotion,
CASE WHEN DEMOGRAPHICS = 'NULL' THEN 'N/A' ELSE DEMOGRAPHICS END AS DEMOGRAPHICS
FROM Dp_Stg1.Person;

INSERT INTO DP_STG2.PhoneNumberType
SELECT DISTINCT
1,
CASE WHEN Name1 = 'NULL' THEN 'N/A' ELSE Name1 END AS Name1
FROM DP_STG1.Person;

INSERT INTO DP_STG2.PersonPhone
SELECT DISTINCT
CASE WHEN a.BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN a.PhoneNumber = 'NULL' THEN -1 ELSE PhoneNumber END AS PhoneNumber,
CASE WHEN b.PhoneNumberTypeID IS NULL THEN -1 ELSE PhoneNumberTypeID END AS PhoneNumberTypeID
FROM DP_STG1.Person a
INNER JOIN
DP_STG2.PhoneNumberType b
ON a.Name1 = b.Name1;

INSERT INTO DP_STG2.StateProvince
SELECT DISTINCT
CASE WHEN StateProvinceID1 = 'NULL' THEN -1 ELSE StateProvinceID1 END AS StateProvinceID,
CASE WHEN StateProvinceCode IS NULL THEN 'N/A' ELSE StateProvinceCode END AS StateProvinceCode,
--StateProvinceCode,
CASE WHEN CountryRegionCode1 = 'NULL' THEN 'N/A' ELSE CAST(CountryRegionCode1 AS CHAR(6)) END AS CountryRegionCode,
CASE WHEN isOnlyStateProvinceFlag = 'NULL' THEN -1 ELSE CAST(isOnlyStateProvinceFlag AS BYTEINT) END AS isOnlyStateProvinceFlag,
CASE WHEN Name2 = 'NULL' THEN 'N/A' ELSE Name2 END AS Name2,
CASE WHEN TerritoryID = 'NULL' THEN -1 ELSE TerritoryID END AS TerritoryID
FROM DP_STG1.Person;
