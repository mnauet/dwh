
INSERT INTO DP_STG2.Department(DepartmentID,DepartmentName,GroupName)
SELECT DISTINCT  1,Name1,GroupName
FROM Dp_Stg1.HR;

INSERT INTO DP_STG2.Shift(ShiftID, Name1,StartTime, EndTime)
SELECT DISTINCT  
1,
		CASE WHEN Name2 ='NULL' THEN 'N/A' ELSE Name2 END AS Name2,
		CAST (SUBSTR(StartTime,1,10) AS TIME),
		CAST (SUBSTR(EndTime,1,10) AS TIME)
FROM Dp_Stg1.HR;

INSERT INTO DP_STG2.Employee(BusinessEntityID, NationalIDNumber,LoginID,
JobTitle,BirthDate, Gender,HireDate,VacationHours,SickLeaveHours)
SELECT DISTINCT  
CASE WHEN BusinessEntityID = 'NULL' THEN -1 ELSE BusinessEntityID END AS BusinessEntityID,
		CASE WHEN NationalIDNumber ='NULL' THEN -1 ELSE NationalIDNumber END AS NationalIDNumber,
		CASE WHEN LoginID = 'NULL' THEN 'N/A' ELSE LoginID END AS LoginID,
		CASE WHEN JobTitle = 'NULL' THEN 'N/A' ELSE JobTitle END AS JobTitle,
		CAST (BirthDate AS DATE FORMAT 'yyyy-mm-dd'),
		CASE WHEN Gender = 'NULL' THEN '-' ELSE Gender END AS Gender,
		CAST (HireDate AS DATE FORMAT 'yyyy-mm-dd'),
		CASE WHEN VacationHours = 'NULL' THEN 0 ELSE VacationHours END AS VacationHours,
		CASE WHEN SickLeaveHours= 'NULL' THEN 0 ELSE SickLeaveHours END AS SickLeaveHours
FROM Dp_Stg1.HR;

INSERT INTO DP_STG2.EmployeeDepartmentHistory(BusinessEntityID,DepartmentID,StartDate,ShiftID,EndDate)

SELECT DISTINCT
CASE WHEN a.BusinessEntityID = 'NULL' THEN -1 ELSE BusinessEntityID END AS BusinessEntityID,
CASE WHEN b.DepartmentID IS NULL THEN -1 ELSE DepartmentID END AS DepartmentID,
CASE WHEN StartDate = 'NULL' THEN  CAST ('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(a.StartDate AS DATE FORMAT 'YYYY-MM-DD') END AS StartDate,
CASE WHEN c.ShiftID IS NULL THEN -1 ELSE ShiftID END AS ShiftID,
CASE WHEN EndDate = 'NULL' THEN  CAST ('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(EndDate AS DATE FORMAT 'YYYY-MM-DD') END AS EndDate
FROM Dp_Stg1.HR a
INNER JOIN
 Dp_Stg2.Department b
ON a.Name1 = b.DepartmentName
INNER JOIN
Dp_Stg2.Shift c
ON a.Name2 = c.Name1;

