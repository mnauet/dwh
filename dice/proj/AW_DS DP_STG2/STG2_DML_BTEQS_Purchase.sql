
--Purchase Module

INSERT INTO DP_STG2.ProductVendor(BusinessEntityID, ProductID, AverageLeadTime,StandardPrice,
LastReceiptCost,LastReceiptDate,MinOrderQty,MaxOrderQty,OnOrderQty,UnitMeasureCode)
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN ProductID1 = 'NULL' THEN -1 ELSE ProductID1 END AS ProductID1,
CASE WHEN AverageLeadTime = 'NULL' THEN -1 ELSE AverageLeadTime END AS AverageLeadTime,
CASE WHEN  StandardPrice = 'NULL' THEN -1 ELSE  CAST(StandardPrice AS DECIMAL(4,2)) END AS  StandardPrice,
CASE WHEN  LastReceiptCost = 'NULL' THEN -1 ELSE  CAST(LastReceiptCost AS DECIMAL(4,2)) END AS LastReceiptCost,
CASE WHEN LastReceiptDate = 'NULL' THEN CAST('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(LastReceiptDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS LastReceiptDate,
CASE WHEN MinOrderQty = 'NULL' THEN -1 ELSE MinOrderQty END AS MinOrderQty,
CASE WHEN MaxOrderQty = 'NULL' THEN -1 ELSE MaxOrderQty END AS MaxOrderQty,
CASE WHEN OnOrderQty = 'NULL' THEN -1 ELSE OnOrderQty END AS OnOrderQty,
CASE WHEN UnitMeasureCode = 'NULL' THEN 'N/A' ELSE CAST(UnitMeasureCode AS CHAR(3)) END AS UnitMeasureCode
FROM Dp_Stg1.Purchase;

INSERT INTO DP_STG2.PurchaseOrderDetail(PurchaseOrderID, PurchaseOrderDetailID,DueDate,
ProductID, OrderQty,UnitPrice,LineTotal, ReceivedQty,RejectedQty,StockedQty)
SELECT DISTINCT
CASE WHEN PurchaseOrderID1 = 'NULL' THEN -1 ELSE PurchaseOrderID1 END AS PurchaseOrderID1,
CASE WHEN PurchaseOrderDetailID = 'NULL' THEN -1 ELSE PurchaseOrderDetailID END AS PurchaseOrderDetailID,
CASE WHEN DueDate = 'NULL' THEN  CAST ('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(DueDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS DueDate,
CASE WHEN ProductID1 = 'NULL' THEN -1 ELSE ProductID1 END AS ProductID1,
CASE WHEN OrderQty = 'NULL' THEN -1 ELSE OrderQty END AS OrderQty,
CASE WHEN UnitPrice = 'NULL' THEN -1 ELSE CAST(UnitPrice AS DECIMAL(7,2)) END AS UnitPrice,
CASE WHEN LineTotal = 'NULL' THEN -1 ELSE CAST( LineTotal AS DECIMAL(9,2)) END AS  LineTotal,
CASE WHEN ReceivedQty = 'NULL' THEN -1 ELSE ReceivedQty END AS ReceivedQty,
CASE WHEN RejectedQty = 'NULL' THEN -1 ELSE RejectedQty END AS RejectedQty,
CASE WHEN StockedQty = 'NULL' THEN -1 ELSE StockedQty END AS StockedQty
FROM Dp_Stg1.Purchase;

INSERT INTO DP_STG2.PurchaseOrderHeader(PurchaseOrderID,ShipMethodID,VendorID,
RevisionNumber,Status,EmployeeID,OrderDate,ShipDate,SubTotal,TaxAmt,Freight,TotalDue)
SELECT DISTINCT
CASE WHEN PurchaseOrderID1 = 'NULL' THEN -1 ELSE PurchaseOrderID1 END AS PurchaseOrderID,
CASE WHEN ShipMethodID1 = 'NULL' THEN -1 ELSE ShipMethodID1 END AS ShipMethodID1,
CASE WHEN VendorID = 'NULL' THEN -1 ELSE VendorID END AS VendorID,
CASE WHEN RevisionNumber = 'NULL' THEN -1 ELSE RevisionNumber END AS RevisionNumber,
CASE WHEN Status = 'NULL' THEN -1 ELSE Status END AS Status,
CASE WHEN EmployeeID = 'NULL' THEN -1 ELSE EmployeeID END AS EmployeeID,
CASE WHEN OrderDate = 'NULL' THEN  CAST ('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(OrderDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS OrderDate,
CASE WHEN ShipDate = 'NULL' THEN  CAST ('1900-01-01' AS DATE FORMAT 'YYYY-MM-DD') ELSE CAST(SUBSTR(ShipDate,1,10) AS DATE FORMAT 'YYYY-MM-DD') END AS ShipDate,
CASE WHEN SubTotal = 'NULL' THEN -1 ELSE CAST(SubTotal AS DECIMAL(9,2)) END AS SubTotal,
CASE WHEN TaxAmt = 'NULL' THEN -1 ELSE CAST(TaxAmt AS DECIMAL(7,2)) END AS TaxAmt,
CASE WHEN Freight = 'NULL' THEN -1 ELSE CAST(Freight AS DECIMAL(7,2)) END AS Freight,
CASE WHEN TotalDue = 'NULL' THEN -1 ELSE CAST(TotalDue AS DECIMAL(9,2)) END AS TotalDue
FROM Dp_Stg1.Purchase;

INSERT INTO DP_STG2.ShipMethod(ShipMethodID, Name1,ShipBase,ShipRate)
SELECT DISTINCT
CASE WHEN ShipMethodID2 = 'NULL' THEN -1 ELSE ShipMethodID2 END AS ShipMethodID2,
CASE WHEN Name2 = 'NULL' THEN 'N/A' ELSE Name2 END AS Name2, 
CASE WHEN ShipBase = 'NULL' THEN -1 ELSE CAST(ShipBase AS DECIMAL(4,2)) END AS ShipBase,
CASE WHEN ShipRate = 'NULL' THEN -1 ELSE CAST(ShipRate AS DECIMAL(4,2)) END AS  ShipRate
FROM Dp_Stg1.Purchase;

INSERT INTO DP_STG2.Vendor(BusinessEntityID, AccountNumber, Name1,CreditRating, PreferredVendorStatus,ActiveFlag,PurchasingWebServiceURL)
SELECT DISTINCT
CASE WHEN BusinessEntityID1 = 'NULL' THEN -1 ELSE BusinessEntityID1 END AS BusinessEntityID1,
CASE WHEN AccountNumber = 'NULL' THEN 'N/A' ELSE AccountNumber END AS AccountNumber,
CASE WHEN Name1 = 'NULL' THEN 'N/A' ELSE Name1 END AS Name1,
CASE WHEN CreditRating = 'NULL' THEN -1 ELSE CreditRating END AS CreditRating,
CASE WHEN PreferredVendorStatus = 'NULL' THEN -1 ELSE PreferredVendorStatus END AS PreferredVendorStatus,
CASE WHEN ActiveFlag = 'NULL' THEN -1 ELSE CAST(ActiveFlag AS BYTEINT) END AS ActiveFlag,
CASE WHEN PurchasingWebServiceURL = 'NULL' THEN 'N/A' ELSE PurchasingWebServiceURL END AS PurchasingWebServiceURL
FROM Dp_Stg1.Purchase;

